FROM ubuntu:18.04

RUN set -xe \
    && apt-get update -y \
    && apt-get install python-pip -y \
    && apt-get install libssl-dev libffi-dev -y
RUN pip install --upgrade pip

RUN pip install flask

LABEL maintainer=abdellatif 
EXPOSE 9000
COPY . .
CMD python html/app.py
